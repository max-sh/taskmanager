package ua.edu.sumdu.java.shestak.view;

import ua.edu.sumdu.java.shestak.notification.Subject;
import ua.edu.sumdu.java.shestak.vieweventsystem.ActionListener;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface View extends Subject {

    public void openView();
    public void closeView();
}
