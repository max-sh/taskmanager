package ua.edu.sumdu.java.shestak.view;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import ua.edu.sumdu.java.shestak.model.core.Task;
import ua.edu.sumdu.java.shestak.notification.*;
import ua.edu.sumdu.java.shestak.notification.Event;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

/**
 * Created by shestak.maksym on 01.03.17.
 */
public class AddEditForm extends AbstractView implements AddEditView {
    private JPanel rootPane;
    private JCheckBox isActiveCheckBox;
    private JTextArea repeatInterval;
    private JButton cancelButton;
    private JButton createButton;
    private JPanel startPanel;
    private JPanel endPanel;
    private JTextField title;

    private JTextField startH;
    private JTextField endH;
    private JTextField endM;
    private JTextField startM;

    private UtilDateModel startModel;
    private UtilDateModel endModel;

    private Event action;



    public AddEditForm() {
        initInterface();
    }

    public static ViewFactory getFactory() {
        return new ViewFactory() {
            @Override
            public View createView() {
                return new AddEditForm();
            }
        };
    }



    private void initInterface() {

        setContentPane(rootPane);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        setSize(new Dimension(600, 200));

        startModel = new UtilDateModel();
        JDatePanelImpl startDatePanel = new JDatePanelImpl(startModel);
        JDatePickerImpl startDatePicker = new JDatePickerImpl(startDatePanel);

        endModel = new UtilDateModel();
        JDatePanelImpl endDatePanel = new JDatePanelImpl(endModel);
        JDatePickerImpl endDatePicker = new JDatePickerImpl(endDatePanel);


        startPanel.setLayout(new GridLayout());
        startPanel.add(startDatePicker);
        endPanel.setLayout(new GridLayout());
        endPanel.add(endDatePicker);

        createButton.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                notifyObservers(Event.ADD_EDIT_VIEW_TASK_ACTION);
            }
        });

        cancelButton.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                notifyObservers(Event.ADD_EDIT_VIEW_CANCEL);
            }
        });

    }


    @Override
    protected void createFrame() {
        setVisible(true);
    }

    @Override
    public void setTitleText(String text) {
        title.setText(text);
    }

    @Override
    public String getTitleText() {
        return title.getText();
    }

    @Override
    public void setStartDate(Date date) {
        startModel.setValue(date);
        startH.setText(String.valueOf(date.getHours()));
        startM.setText(String.valueOf(date.getMinutes()));
    }

    @Override
    public Date getStartDate() {
        Date date = startModel.getValue();
        date.setHours(Integer.parseInt(startH.getText()));
        date.setMinutes(Integer.parseInt(startM.getText()));
        return date;
    }

    @Override
    public void setEndDate(Date date) {
        endModel.setValue(date);
        endH.setText(String.valueOf(date.getHours()));
        endM.setText(String.valueOf(date.getMinutes()));
    }

    @Override
    public Date getEndDate() {
        Date date = endModel.getValue();
        date.setHours(Integer.parseInt(endH.getText()));
        date.setMinutes(Integer.parseInt(endM.getText()));
        return date;
    }

    @Override
    public void setActiveState(boolean state) {
        isActiveCheckBox.setSelected(state);
    }

    @Override
    public boolean getActiveState() {
        return isActiveCheckBox.isSelected();
    }

    @Override
    public void setRepeatInterval(long repeatInterval) {
        this.repeatInterval.setText(String.valueOf(repeatInterval));
    }

    @Override
    public long getRepeatInterval() {
        return Long.parseLong(repeatInterval.getText());
    }

    @Override
    public Event getAction() {
        return action;
    }

    @Override
    public void setAction(Event action) {
        this.action = action;
        if(action == Event.EDIT_TASK) createButton.setText("Edit");
        else createButton.setText("Add");
    }
}
