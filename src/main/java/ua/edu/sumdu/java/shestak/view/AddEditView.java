package ua.edu.sumdu.java.shestak.view;

import ua.edu.sumdu.java.shestak.notification.Event;

import java.util.Date;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface AddEditView extends View {

    public void setTitleText(String text);
    public String getTitleText();

    public  void setStartDate(Date date);
    public Date getStartDate();

    public void setEndDate(Date date);
    public Date getEndDate();

    public void setActiveState(boolean state);
    public boolean getActiveState();
    public void setRepeatInterval(long repeatInterval);
    public long getRepeatInterval();

    public Event getAction();
    public void setAction(Event action);
}
