package ua.edu.sumdu.java.shestak.model;

import ua.edu.sumdu.java.shestak.model.core.AbstractTaskList;
import ua.edu.sumdu.java.shestak.model.core.Task;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Set;
import java.util.SortedMap;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface TaskModel {

    public void readFromFile(File file) throws IOException, ParseException;
    public void writeToFile(File file) throws IOException;
    public void addTask(Task task);
    public void removeTask(Task task);
    public AbstractTaskList getTasks();
    public SortedMap<Date, Set<Task>> getCallendar(Date from, Date to);
    public Task get(int index);
}
