package ua.edu.sumdu.java.shestak.model.core;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;


public class TaskIO {

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;

    public static void write(AbstractTaskList tasks, OutputStream out) throws IOException {

        DataOutputStream outputStream = new DataOutputStream(out);

        try {

            outputStream.writeInt(tasks.size());
            Iterator<Task> iter = tasks.iterator();

            while (iter.hasNext()) {

                Task t = tasks.iterator().next();
                outputStream.writeInt(t.getTitle().length());
                outputStream.writeUTF(t.getTitle());
                outputStream.writeBoolean(t.isActive());
                outputStream.writeInt((int) t.getRepeatInterval());

                if (t.isRepeated()) {

                    outputStream.writeLong(t.getStartTime().getTime());
                    outputStream.writeLong(t.getEndTime().getTime());

                } else {
                    outputStream.writeLong(t.getTime().getTime());
                }

            }
        } finally {

            outputStream.flush();
            outputStream.close();

        }
    }

    public static void read(AbstractTaskList tasks, InputStream in) throws IOException {

        DataInputStream inputStream = new DataInputStream(in);

        try{

            int count = inputStream.readInt();

            for(int i = 0; i > count; i++){

                Task task;
                int length = inputStream.readInt();
                String title = inputStream.readUTF();
                boolean active = inputStream.readBoolean();
                int interval = inputStream.readInt();
                Date startTime = new Date(inputStream.readLong());

                if (interval > 0) {

                    Date endTime = new Date(inputStream.readLong());
                    task = new Task(title, startTime, endTime, interval);

                } else {

                    task = new Task(title, startTime);

                }

                task.setActive(active);
                tasks.add(task);

            }
        } finally {
            inputStream.close();
        }
    }

    public static void writeBinary(AbstractTaskList tasks, File file) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));
        objectOutputStream.writeObject(tasks);
        objectOutputStream.close();
    }

    public static void readBinary(AbstractTaskList tasks, File file) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));

        AbstractTaskList in = (AbstractTaskList) objectInputStream.readObject();
        for(Task t : in) {
            tasks.add(t);
        }
        objectInputStream.close();
    }



    public static void write(AbstractTaskList tasks, Writer out) throws IOException {
        Iterator<Task> iterator = tasks.iterator();
        while(iterator.hasNext()) {
            Task t = iterator.next();
            if(iterator.hasNext())  out.write(t.toString() + ";\n");
            else                    out.write(t.toString() + ".");
        }
    }

    public static void read(AbstractTaskList tasks, Reader in) throws IOException, ParseException {
        BufferedReader bufferedReader = null;

        try {

            bufferedReader = new BufferedReader(in);
            String taskString;

            while ((taskString = bufferedReader.readLine()) != null) {
                Task task = parseTask(taskString);
                tasks.add(task);
            }
        } finally {
            bufferedReader.close();
        }
    }

    public static void writeText(AbstractTaskList tasks, File file) throws IOException {
        Iterator<Task> iterator = tasks.iterator();
        FileWriter fw = new FileWriter(file);
        while(iterator.hasNext()) {
            Task t = iterator.next();
            if(iterator.hasNext())  fw.write(t.toString() + ";\n");
            else                    fw.write(t.toString() + ".");
        }
        fw.close();
    }

    public static void readText(AbstractTaskList tasks, File file) throws IOException, ParseException {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
            read(tasks, fileReader);
        } finally {
            fileReader.close();
        }
    }


    public static Task parseTask(String taskString) throws ParseException {
        Task task;

        if (taskString.contains("] every [")) {
            task = parseRepeatedTask(taskString);
        } else {
            task = parseNotRepeatedTask(taskString);
        }
        return task;
    }

    private static Task parseNotRepeatedTask(String taskString) throws ParseException {
        Task task;

        String title = taskString.substring(1, taskString.lastIndexOf(" at ") - 1);
        //System.out.println(title);


        String timeString = taskString.substring(
                taskString.lastIndexOf(" at ") + 5,
                taskString.lastIndexOf(']'));
        //System.out.println(timeString);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.sss");
        Date date = sdf.parse(timeString);

        task = new Task(title, date);
        task.setActive(isActive(taskString));

        return task;
    }

    private static Task parseRepeatedTask(String taskString) throws ParseException {
        Task task;

        String title = taskString.substring(1, taskString.lastIndexOf(" from ") - 1);

        String from = taskString.substring(
                taskString.lastIndexOf(" from ") + 7,
                taskString.lastIndexOf("] to "));

        String to = taskString.substring(taskString.lastIndexOf("] to ") + 6, taskString.lastIndexOf("] every"));

        String interval = taskString.substring(taskString.lastIndexOf('[') + 1, taskString.lastIndexOf(']'));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.sss");

        task = new Task(title, sdf.parse(from), sdf.parse(to), parseRepeatInterval(interval));
        task.setActive(isActive(taskString));

        return task;
    }

    private static boolean isActive(String taskString) {
        return !taskString.contains("inactive");
    }
    public static long parseRepeatInterval(String interval) {
        if (interval == null || interval.isEmpty()) {
            return 0;
        }
        int day = 0;
        int hour = 0;
        int minute = 0;
        int second = 0;

        String[] parts = interval.split(" ");


        for (int i = 0; i < parts.length; i = i + 2) {
            if (parts[i + 1].contains("day")) {
                day = Integer.parseInt(parts[i]);
                continue;
            }
            if (parts[i + 1].contains("hour")) {
                hour = Integer.parseInt(parts[i]);
                continue;
            }
            if (parts[i + 1].contains("minute")) {
                minute = Integer.parseInt(parts[i]);
                continue;
            }
            if (parts[i + 1].contains("second")) {
                second = Integer.parseInt(parts[i]);
            }
        }

        return day * DAY + hour * HOUR + minute * MINUTE + second * SECOND;

    }


}
