package ua.edu.sumdu.java.shestak.model;

import ua.edu.sumdu.java.shestak.model.core.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public class SimpleTaskModel implements TaskModel {

    private AbstractTaskList tasks;

    public SimpleTaskModel() {
        tasks = new LinkedTaskList();
    }

    public SimpleTaskModel(AbstractTaskList tasks) {
        this.tasks = tasks;
    }

    @Override
    public void readFromFile(File file) throws IOException, ParseException {
        TaskIO.readText(tasks, file);
    }

    @Override
    public void writeToFile(File file) throws IOException {
        TaskIO.writeText(tasks, file);
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    @Override
    public void removeTask(Task task) {
        tasks.remove(task);
    }

    @Override
    public AbstractTaskList getTasks() {
        return tasks;
    }

    @Override
    public SortedMap<Date, Set<Task>> getCallendar(Date from, Date to) {
        return Tasks.calendar(tasks, from, to);
    }

    @Override
    public Task get(int index) {
        Iterator<Task> iterator = tasks.iterator();
        for(int i = 0; i < index; i++){
            iterator.next();
        }
        return iterator.next();
    }
}
