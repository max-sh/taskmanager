package ua.edu.sumdu.java.shestak.presenter;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface Presenter {
    public void startApp();
}
