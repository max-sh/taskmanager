package ua.edu.sumdu.java.shestak.view;

import ua.edu.sumdu.java.shestak.notification.Event;
import ua.edu.sumdu.java.shestak.notification.Observer;
import ua.edu.sumdu.java.shestak.notification.SimpleSubject;
import ua.edu.sumdu.java.shestak.notification.Subject;
import ua.edu.sumdu.java.shestak.vieweventsystem.ActionListener;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public abstract class AbstractView extends JFrame implements View {



    private List<Observer> observers = new ArrayList<>();
    private String message;
    private boolean changed;
    private final Object MUTEX = new Object();


    @Override
    public void register(Observer obj) {
        if(obj == null) throw new NullPointerException("Null Observer");
        synchronized(MUTEX) {
            if(!observers.contains(obj)) observers.add(obj);
        }
    }

    @Override
    public void unregister(Observer obj) {
        synchronized(MUTEX) {
            observers.remove(obj);
        }
    }

    @Override
    public void notifyObservers(Event event) {


//        List<Observer> observersLocal = null;
//        //synchronization is used to make sure any observer registered after message is received is not notified
//        synchronized(MUTEX) {
//            if(!changed)
//                return;
//            observersLocal = new ArrayList<>(this.observers);
//            this.changed = false;
//        }

        for(Observer obj : observers) {
            obj.update(event);
        }

    }

    @Override
    public Object getUpdate(Observer obj) {
        return this.message;
    }


    @Override
    public void openView() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createFrame();
            }
        });
    }

    @Override
    public void closeView() {
        setVisible(false);
        dispose();
    }

    protected abstract void createFrame();

}
