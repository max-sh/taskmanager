package ua.edu.sumdu.java.shestak.view;

import net.sourceforge.jdatepicker.impl.JDatePanelImpl;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;
import net.sourceforge.jdatepicker.impl.UtilDateModel;
import ua.edu.sumdu.java.shestak.model.core.AbstractTaskList;
import ua.edu.sumdu.java.shestak.model.core.Task;
import ua.edu.sumdu.java.shestak.notification.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

/**
 * Created by shestak.maksym on 01.03.17.
 */
public class CalendarForm extends AbstractView implements CalendarView {

    private JPanel root;
    private JTable table;
    private JPanel beginDateP;
    private JPanel endDateP;
    private JButton showButton;

    private UtilDateModel startModel;
    private UtilDateModel endModel;


    public CalendarForm() {
        initInterface();
    }

    private void initInterface() {

        setContentPane(root);
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);

        startModel = new UtilDateModel();
        JDatePanelImpl startDatePanel = new JDatePanelImpl(startModel);
        JDatePickerImpl startDatePicker = new JDatePickerImpl(startDatePanel);

        endModel = new UtilDateModel();
        JDatePanelImpl endDatePanel = new JDatePanelImpl(endModel);
        JDatePickerImpl endDatePicker = new JDatePickerImpl(endDatePanel);


        beginDateP.setLayout(new GridLayout());
        beginDateP.add(startDatePicker);
        endDateP.setLayout(new GridLayout());
        endDateP.add(endDatePicker);

        showButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                notifyObservers(ua.edu.sumdu.java.shestak.notification.Event.SHOW_CALENDAR_TASKS);
            }
        });
    }

    @Override
    protected void createFrame() {
        setVisible(true);
    }

    public static ViewFactory getFactory() {
        return new ViewFactory() {
            @Override
            public View createView() {
                return new CalendarForm();
            }
        };
    }

    @Override
    public void showTasks(AbstractTaskList tasks) {
        String[] columnNames = {"Title", "Begin", "End"};

        DefaultTableModel dtm = new DefaultTableModel(0, 0);
        dtm.setColumnIdentifiers(columnNames);

        table.setModel(dtm);
        table.setFillsViewportHeight(true);

        for(Task task : tasks) {
            dtm.addRow(new String[]{task.getTitle(), task.getStartTime().toString(), task.getEndTime().toString()});
        }
    }

    @Override
    public void clearTasksTable() {
        DefaultTableModel dm = (DefaultTableModel) table.getModel();
        int rowCount = dm.getRowCount();
        //Remove rows one by one from the end of the table
        for (int i = rowCount - 1; i >= 0; i--) {
            dm.removeRow(i);
        }
    }

    @Override
    public Date getBeginDate() {
        return startModel.getValue();
    }

    @Override
    public Date getEndDate() {
        return endModel.getValue();
    }
}
