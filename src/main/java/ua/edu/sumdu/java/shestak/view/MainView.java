package ua.edu.sumdu.java.shestak.view;

import ua.edu.sumdu.java.shestak.model.core.AbstractTaskList;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface MainView extends View {


    public void showAllTasks(AbstractTaskList tasks);
    public void setTitleText(String text);
    public void setBeginTimeText(String text);
    public void setEndTimeText(String text);
    public void setActiveStateText(String text);
    public void setRepeatIntervalText(String text);
    public int getSelectedRow();
    public void clearTasksTable();

}
