package ua.edu.sumdu.java.shestak.notification;

/**
 * Created by shestak.maksym on 05.03.17.
 */
public interface Subject {

    //methods to register and unregister observers
    public void register(Observer obj);
    public void unregister(Observer obj);

    //method to notify observers of change
    public void notifyObservers(Event event);

    //method to get updates from subject
    public Object getUpdate(Observer obj);
}
