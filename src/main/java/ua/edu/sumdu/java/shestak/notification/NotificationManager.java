package ua.edu.sumdu.java.shestak.notification;

import org.apache.log4j.Logger;
import ua.edu.sumdu.java.shestak.model.core.AbstractTaskList;
import ua.edu.sumdu.java.shestak.model.core.Tasks;
import ua.edu.sumdu.java.shestak.presenter.AppPresenter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by shestak.maksym on 05.03.17.
 */
public class NotificationManager extends Thread implements Subject{


    private long notificationPeriod;
    AbstractTaskList tasks;
    private boolean isRunning;

    private List<Observer> observers = new ArrayList<>();
    private String message;
    private boolean changed;
    private final Object MUTEX = new Object();
    private AbstractTaskList nearestTasks;

    private final static Logger logger = Logger.getLogger(NotificationManager.class);

    public NotificationManager() {

    }
    public NotificationManager(AbstractTaskList tasks, long notificationPeriod, Observer observer) {
        this.tasks = tasks;
        this.notificationPeriod = notificationPeriod;
        register(observer);
    }

    @Override
    public void run() {
        isRunning = true;

        while(isRunning) {

            Date currentTime = new Date();
            nearestTasks  = (AbstractTaskList) Tasks.incoming(tasks, currentTime, new
                    Date(currentTime.getTime() + notificationPeriod));

            if (nearestTasks.size() != 0) {
                notifyObservers(Event.NOTIFICATION);
            }

            logger.info("neares tasks " + nearestTasks.size());

            try {
                Thread.sleep(notificationPeriod);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    public void stopManager() {
        isRunning = false;
    }

    public AbstractTaskList getNearestTasks() {
        return nearestTasks;
    }

    public void setTasks(AbstractTaskList tasks) {
        this.tasks = tasks;
    }
    public void setNotificationPeriod(long notificationPeriod) {
        this.notificationPeriod = notificationPeriod;
    }

    @Override
    public void register(Observer obj) {
        if(obj == null) throw new NullPointerException("Null Observer");
        synchronized(MUTEX) {
            if(!observers.contains(obj)) observers.add(obj);
        }
    }

    @Override
    public void unregister(Observer obj) {
        synchronized(MUTEX) {
            observers.remove(obj);
        }
    }

    @Override
    public void notifyObservers(Event event) {


//        List<Observer> observersLocal = null;
//        //synchronization is used to make sure any observer registered after message is received is not notified
//        synchronized(MUTEX) {
//            if(!changed)
//                return;
//            observersLocal = new ArrayList<>(this.observers);
//            this.changed = false;
//        }

        for(Observer obj : observers) {
            obj.update(event);
        }

    }

    @Override
    public Object getUpdate(Observer obj) {
        return this.message;
    }
}
