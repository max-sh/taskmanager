package ua.edu.sumdu.java.shestak.view;

import ua.edu.sumdu.java.shestak.model.core.AbstractTaskList;
import ua.edu.sumdu.java.shestak.model.core.Task;

import javax.swing.table.DefaultTableModel;
import java.util.Date;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public interface CalendarView extends View {

    public void showTasks(AbstractTaskList tasks);
    public void clearTasksTable();
    public Date getBeginDate();
    public Date getEndDate();
}
