package ua.edu.sumdu.java.shestak.notification;

/**
 * Created by shestak.maksym on 05.03.17.
 */
public interface Observer {

    //method to update the observer, used by subject
    public void update(Event event);

}
