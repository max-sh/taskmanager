package ua.edu.sumdu.java.shestak.presenter;

import ua.edu.sumdu.java.shestak.model.SimpleTaskModel;
import ua.edu.sumdu.java.shestak.model.TaskModel;
import ua.edu.sumdu.java.shestak.model.core.*;
import ua.edu.sumdu.java.shestak.notification.Event;
import ua.edu.sumdu.java.shestak.notification.NotificationManager;
import ua.edu.sumdu.java.shestak.notification.Observer;
import ua.edu.sumdu.java.shestak.view.*;

/**
 * Created by shestak.maksym on 15.03.17.
 */
import org.apache.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public class AppPresenter implements Presenter, Observer {

    private TaskModel model;
    private View mainView;
    private View addEditView;
    private View calendarView;
    private NotificationManager notificationManager;
    private final static Logger logger = Logger.getLogger(AppPresenter.class);

    private static final int SECOND = 1000;
    private static final int MINUTE = 60 * SECOND;
    private static final int HOUR = 60 * MINUTE;
    private static final int DAY = 24 * HOUR;

    public AppPresenter() {
        createViews();
        model = new SimpleTaskModel();
        notificationManager = new NotificationManager(model.getTasks(), MINUTE, this);
        notificationManager.start();
    }

    private void createViews() {
        mainView = createView(MainForm::new);
        addEditView = createView(AddEditForm::new);
        calendarView = createView(CalendarForm::new);
    }

    @Override
    public void startApp() {
        mainView.openView();
//        ((MainView) mainView).showAllTasks(model.getTasks());

    }

    private View createView(ViewFactory factory) {
        View view = factory.createView();
        view.register(this);
        return view;
    }

    @Override
    public void update(Event event) {
        logger.info(event);
        switch(event) {
            case MAIN_ADD_TASK:
                addTask();
                break;
            case TASK_SELECTED:
                taskSelected();
                break;
            case EDIT_TASK:
                editTask();
                break;
            case DELETE_TASK:
                deleteTask();
                break;
            case ADD_EDIT_VIEW_TASK_ACTION:
                processTasK();
                break;
            case OPEN_CALENDAR:
                openCalendar();
                break;
            case SHOW_CALENDAR_TASKS:
                showCalendarTasks();
                break;
            case ADD_EDIT_VIEW_CANCEL:
                addEditView.closeView();
                break;
            case OPEN_FILE:
                openFile();
                break;
            case SAVE_FILE:
                saveFile();
                break;
            case NOTIFICATION:
                showNotification();
                break;
            default: break;
        }
    }

    private void showNotification() {
        AbstractTaskList tasks = notificationManager.getNearestTasks();
        StringBuilder stringBuilder = new StringBuilder();

        for(Task t : tasks) {
            stringBuilder.append(t.toString());
            stringBuilder.append('\n');
        }
        JOptionPane.showMessageDialog(new JFrame(), stringBuilder.toString(), "Nearest tasks", JOptionPane.INFORMATION_MESSAGE);
    }

    private void saveFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        fileChooser.setDialogTitle("Specify a file to save");

        int userSelection = fileChooser.showSaveDialog(new JFrame());

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();

            try {
                model.writeToFile(fileToSave);
            } catch(IOException e) {
//                e.printStackTrace();
                JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void openFile() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        int result = fileChooser.showOpenDialog(new JFrame());
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();

            try {

                model.readFromFile(selectedFile);
                notificationManager.setTasks(model.getTasks());

                ((MainView) mainView).showAllTasks(model.getTasks());
            } catch(IOException | ParseException e) {
                //e.printStackTrace();
                JOptionPane.showMessageDialog(new JFrame(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
            //System.out.println("Selected file: " + selectedFile.getAbsolutePath());
        }
    }

    private void showCalendarTasks() {

        if(model.getTasks().size() == 0) {
            JOptionPane.showMessageDialog(new JFrame(), "Empty task list", "Info", JOptionPane.INFORMATION_MESSAGE);
        }
        else {
            AbstractTaskList calendar = (AbstractTaskList) Tasks.incoming(model.getTasks(),
                    ((CalendarView) calendarView).getBeginDate(),
                    ((CalendarView) calendarView).getEndDate());
            System.out.println(model.getTasks().size());
            ((CalendarView) calendarView).showTasks(calendar);
        }


    }

    private void openCalendar() {
        calendarView.openView();
    }

    private void processTasK() {

        Task task;
        if(((AddEditView) addEditView).getAction() == Event.ADD_TASK) {
            task = new Task();
        }
        else {
            task = model.get(((MainView) mainView).getSelectedRow());

        }

        try {
            task.setTitle(((AddEditForm) addEditView).getTitleText());
            task.setActive(((AddEditForm) addEditView).getActiveState());

            if(((AddEditForm) addEditView).getRepeatInterval() != 0) {
                task.setTime(((AddEditForm) addEditView).getStartDate(),
                        ((AddEditForm) addEditView).getEndDate(),
                        ((AddEditForm) addEditView).getRepeatInterval());
            } else {
                task.setTime(((AddEditForm) addEditView).getStartDate());
            }

            if(((AddEditView) addEditView).getAction() == Event.ADD_TASK)
                model.addTask(task);


            ((MainView) mainView).clearTasksTable();
            ((MainView) mainView).showAllTasks(model.getTasks());
            addEditView.closeView();

        } catch(NumberFormatException e) {
            JOptionPane.showMessageDialog(new JFrame(), "Fill empty fields", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    private void addTask(){
        ((AddEditView) addEditView).setAction(Event.ADD_TASK);
        addEditView.openView();

    }
    private void taskSelected() {
        int index = ((MainView) mainView).getSelectedRow();
        Task selectedTask = model.get(index);
        ((MainView) mainView).setTitleText(selectedTask.getTitle());
        ((MainView) mainView).setBeginTimeText(selectedTask.getStartTime().toString());
        ((MainView) mainView).setEndTimeText(selectedTask.getEndTime().toString());
        ((MainView) mainView).setRepeatIntervalText(String.valueOf(selectedTask.getRepeatInterval()));
        ((MainView) mainView).setActiveStateText(String.valueOf(selectedTask.isActive()));
    }
    private void editTask() {

        Task task = model.get(((MainView) mainView).getSelectedRow());
        ((AddEditView) addEditView).setAction(Event.EDIT_TASK);
        addEditView.openView();

        ((AddEditView) addEditView).setTitleText(task.getTitle());
        ((AddEditView) addEditView).setStartDate(task.getStartTime());
        ((AddEditView) addEditView).setEndDate(task.getEndTime());
        ((AddEditView) addEditView).setActiveState(task.isActive());
        ((AddEditView) addEditView).setRepeatInterval(task.getRepeatInterval());

    }
    private void deleteTask() {
        int index = ((MainView) mainView).getSelectedRow();
        model.removeTask(model.get(index));
        ((MainView) mainView).clearTasksTable();
        ((MainView) mainView).showAllTasks(model.getTasks());

        ((MainView) mainView).setTitleText("");
        ((MainView) mainView).setBeginTimeText("");
        ((MainView) mainView).setEndTimeText("");
        ((MainView) mainView).setRepeatIntervalText("");
        ((MainView) mainView).setActiveStateText("");
    }




    public static void main(String[] args) {
        Presenter presenter = new AppPresenter();
        presenter.startApp();
    }
}
