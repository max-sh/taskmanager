package ua.edu.sumdu.java.shestak.notification;

/**
 * Created by shestak.maksym on 15.03.17.
 */
public enum Event {
    TASK_SELECTED,
    MAIN_ADD_TASK,
    EDIT_TASK,
    DELETE_TASK,
    OPEN_CALENDAR,
    ADD_EDIT_VIEW_TASK_ACTION,
    SHOW_CALENDAR_TASKS, ADD_TASK, SAVE_FILE, OPEN_FILE, NOTIFICATION, ADD_EDIT_VIEW_CANCEL

}
