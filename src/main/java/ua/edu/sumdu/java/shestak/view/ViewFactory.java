package ua.edu.sumdu.java.shestak.view;

/**
 * Created by shestak.maksym on 16.03.17.
 */
public interface ViewFactory {
    public View createView();
}
